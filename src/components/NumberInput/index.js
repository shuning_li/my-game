import React from 'react';
import './style.less';

const NumberInput = props => {
  const { numbers, handleChange } = props;
  return (
    <section className="number-input">
      {numbers.map((item, index) => (
        <input key={index} maxLength={1} onChange={e => handleChange(index, e.target.value)} value={item} type="text"/>
      ))}
    </section>
  )
};

export default NumberInput;