import React from 'react';
import './style.less';

const ResultList = props => {
  return (
    <section className="game-result">
      <ul>
        {props.result.map((item, index) => (
          <li key={index}>
            <section>{item.answer}</section>
            <section>{item.hint}</section>
          </li>
        ))}
      </ul>
    </section>
  )
};

export default ResultList;