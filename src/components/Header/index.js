import React from 'react';
import './style.less';

const Header = props => {
  return (
    <header className="game-header">
      <button onClick={props.handleClick}>New Game</button>
    </header>
  )
};

export default Header;