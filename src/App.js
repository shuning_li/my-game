import React from 'react';
import Header from './components/Header';
import NumberInput from './components/NumberInput';
import ResultList from './components/ResultList';
import Congratulation from './components/Congratulation';
import * as API from './api';
import './App.less';
import Footer from './components/Footer';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      correct: false,
      location: '',
      numbers: new Array(4).fill(''),
      result: []
    };
    this.createNewGame = this.createNewGame.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
    this.handleAnswerSubmit = this.handleAnswerSubmit.bind(this);
  }

  createNewGame() {
    this.setState({
      correct: false,
      location: '',
      numbers: new Array(4).fill(''),
      result: []
    });
    API.createGame().then(res => {
      this.setState({
        location: res
      })
    })
  }

  handleNumberChange(index, val) {
    const numbers = [...this.state.numbers];
    numbers[index] = val;
    this.setState({
      numbers
    });
  }

  handleAnswerSubmit(e) {
    e.preventDefault();
    const answer = this.state.numbers.join('');
    API.guessGame(this.state.location, answer)
      .then(res => {
        const { hint, correct } = res;
        this.setState({
          correct,
          result: [...this.state.result, {
            answer,
            hint
          }]
        })
      })
  }

  render() {
    const { correct, numbers, result } = this.state;
    return (
      <main className="game-content">
        <section className="left">
          <Header handleClick={this.createNewGame} />
          <section>
            <form onSubmit={this.handleAnswerSubmit}>
              <NumberInput handleChange={this.handleNumberChange} numbers={numbers}/>
              <ResultList result={result}/>
              <Footer/>
            </form>
          </section>
        </section>
        <section className="right">
          {correct && <Congratulation/>}
        </section>
      </main>
    )
  }
}

export default App;