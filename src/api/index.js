const BASE_URL = 'http://localhost:8080';
const PATH = '/api/games'

export const createGame = () => {
  return fetch(`${BASE_URL}${PATH}`, {
    method: 'POST'
  }).then(res => res.headers.get('Location'));
};

export const getGameDetail = id => {
  return fetch(`${BASE_URL}${PATH}/${id}`, {
    method: 'GET'
  }).then(res => res.json())
};

export const guessGame = (location, answer) => {
  return fetch(`${BASE_URL}${location}`, {
    method: 'PATCH',
    headers:{
      'Content-Type': 'application/json'
    },
    body:JSON.stringify({
      answer
    })
  }).then(res => res.json());
};
